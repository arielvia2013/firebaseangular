import { Component,OnInit } from '@angular/core';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements	OnInit {
  
  closeResult = '';
  estudianteForm: FormGroup;

  constructor(
    private modalService: NgbModal,
    public fb:FormBuilder
    ) {}

  config:any;
  colletion = {count:0, data:[]}
  ngOnInit(): void {

    this.config={
      itemsPerPage:5,
      currentPage:1,
      totalItems:this.colletion.count
    };

    this.estudianteForm=this.fb.group({
      id:['',Validators.required],
      nombre:['',Validators.required],
      apellido:['',Validators.required],
    })

    for(var i=0; i < 5; i++){
      this.colletion.data.push({
        id:i,
        nombre:"nombre"+i,
        apellido:"apellido"+i
      })
    }

    

    
  }
  pageChanged(event){
    this.config.currentPage=event;
  }
  eliminar(item:any):void{
    this.colletion.data.pop(item);
  }
  guardarEstudiante(){
    this.colletion.data.push(this.estudianteForm.value);
    this.modalService.dismissAll();
  }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  
}
